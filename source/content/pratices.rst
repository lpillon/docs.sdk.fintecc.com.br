.. _pratices:

Boas Práticas
=============
Leia atentamente cada paragrafo dessa sessão pois é de insuma importância para que você possa contribuir para o projeto.
Além de ajudar no entendimento do código.

Providers
^^^^^^^^^
Caso você crie um novo provider ou utilize algum provider de terceiros ele deve ser registrado no ``Fintecc\Providers\MainServiceProvider``
assim todas as versões do láravel poderão utilizar o seu provider automáticamente após a instalção do SDK. veja: :ref:`instalation`

.. code-block:: php

    <?php
    /** @var array PROVIDERS define os providers a serem registrados na aplicação */
    const PROVIDERS = [
        \Namespace\Para\Seu\Provider::class,
    ];


Blocos de comentários
^^^^^^^^^^^^^^^^^^^^^
Todo código no SDK está comentando utilizando os padrões do PHP Documentor.
Você pode consultar a `PSR do PHP Documentor`_ para saber exatamento como comentar seu código.

Nomenclaturas
^^^^^^^^^^^^^
Os padrões definidos pelo projeto são:

* Nome de varíaveis e propriedades de classes devem seguir o padrão *camelCase*
* Constantes devem ser definidas com todas as letras maiuscúlas e palavras devem ser separadas por underscore (*CONSTANTE_NOME*)

O restante do código **DEVE** seguir os padrões definidos na PSR-2, caso você tenha alguma dúvida sobre PSR,
consulte o guia `PHP Do Jeito Certo`_.

TAGs
^^^^
As tags do projeto devem ser númeradas de uma forma que seja consistente com o `Versionamento Semântico`_

.. _`PSR do PHP Documentor`: https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md
.. _`PHP Do Jeito Certo`: http://br.phptherightway.com/
.. _`Versionamento Semântico`: https://semver.org/lang/pt-BR/
