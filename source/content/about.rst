.. _about:

Sobre
=====
Entender um pouco sobre o objetivo desse projeto é essencial para começar a utilizá-lo.

Introdução
^^^^^^^^^^
Esse SDK visa facilitar a implementação de projetos Fintecc disponibilizando recursos básicos
e comuns as aplicaçẽos em um único lugar.
Com isso é possível usar classes básicas de autenticação ou cadastro, por exemplo,
sem a necessidade de escrever toda uma implementação nova para cada empresa/projeto.

Por que utilizar
^^^^^^^^^^^^^^^^
Esse projeto pode agilizar e muito a implementação de novas features e a manutenção das já existentes.
Utilize esse SDK para todos os projetos que precisam utilizar qualquer API da Fintecc, a mesma estrutura de autenticação,
conexão com banco de dados ou simplesmente rápida referência para esses propósitos.