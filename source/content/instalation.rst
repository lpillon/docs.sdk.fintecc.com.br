.. _instalation:

Instalação
==========
Os passos a seguir mostram a forma básica para começar a utilizar a Fintecc SDK

Composer
^^^^^^^^
Esse SDK deve ser instalado diretamente pelo composer,
por se tratar de um repositório VCS privado você deve inserir a referência desse repositório no *composer.json* do seu projeto:

.. code::

    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:ambientedevfintecc/sdk.fintecc.com.br.git"
        }
    ]

E então utilizar o comando ``composer required fintecc/sdk``

Ou inserir a dependência diretamento no arquivo *composer.json*

.. code::

    "require": {
        "fintecc/sdk": "1.*"
    },

.. note::
    Para instalar o fintecc/sdk a partir de um branch (não uma tag) coloque o prefixo **dev-**
    seguido do nome do branch desejado, como **dev-bug_fix** por exemplo.

Em seguida bastar executar ``composer update fintecc/sdk``

E o SDK já estará disponível para utilização

para mais informações veja a documentação do `composer para repositórios VCS`_

Inicialização
^^^^^^^^^^^^^
A inicialização dos provideres pode variar de acordo com a versão do Laravel,
veja como iniciar os providers para cada versão:

Laravel 5.5 e superiores
""""""""""""""""""""""""
O Laravel vai identificar a referências dos providers automáticamente

Laravel 5.4 e inferiores
""""""""""""""""""""""""
Basta fazer a referência de um único provider no arquivo *config/app.php*

.. code-block:: php

    <?php
    'providers' => [
        Fintecc\Providers\MainServiceProvider::class
    ]

Publicando
^^^^^^^^^^
Para publicar as configurações do SDK para sua aplicação basta executar ``php artisan vendor:publish`` no diretório do projeto


.. _`composer para repositórios VCS`: https://getcomposer.org/doc/05-repositories.md#vcs