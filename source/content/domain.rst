.. _domain:

Domain
======
Domains são classes semânticas que contem um conjunto de informações que possuem, de alguma forma, valor para o sistema.

Um bom exemplo de domain é a classe de ``Cliente`` que possui um conjunto de informações como nome, email e cpf, por exemplo.

A estrutura de um domain consistem simplesmente em propriedades publicas, por exemplo:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $id token de sessão do cliente
        * @translate clienteTokenId
        */
        public $id;

        /**
        * @var int $pureId id sequencial do cliente
        * @translate id
        */
        public $pureId;

        /**
        * @var string $status status atual do cliente
        * @translate statusId
        */
        public $status;

        /**
        * @var string $name nome completo do cliente
        * @translate pessoa->nome
        */
        public $name;
    }

Criando um domain
^^^^^^^^^^^^^^^^^
Para criar um domain você pode usar o método ``create`` da classe ``Fintecc\Domain\Domain``,
que recebe como parametro a classe que o deve ser instanciada, as informações a serem inseridas no domain
e um boolean que define se as propriedades devem ser traduzidas com o padrão definido em ``@translate`` de cada propriedade

.. code-block:: php

    <?php
    Domain::create(MeuDomain::class, $data, true);

@translate
^^^^^^^^^^
Ao colocar a tag ``@translate`` no comentário de sua propriedade você está dizendo ao ``Fintecc\Domain\Domain``
como definir essa propriedade quando vinda de alguma lugar que deve ser traduzida.

Por exemplo, digamos que ao fazer um **GET**  de um usuário na API a propriedade *name* venha como *nomeDoUsuario*,
colocando ``@translate nomeDoUsuario`` no comentário da propriedade já faz com ela seja traduzida ao ser criada.

Caso você queira traduzir a propriedade a partir de um array associativo ou de um objeto basta utilizar o operador de objeto ``->``.
Por exemplo, ``@translate usuario->nome``. E isso pode ocorrer de forma recursiva: ``@translate usuario->dados->detalhes->nome``

Caso sua propriedade consiste na concatenação de duas ou mais propriedades do seu array associativo/objeto
você pode dizer isso ao ``create`` ao inserir o operador de adição ``+`` no ``@translate`` da sua propriedade.
Por exemplo:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $phone numero de telefone do cliente
        * @translate telefone->ddd + telefone->numero
        */
        public $phone;
    }

O exemplo acima vai inserir o que estiver dentro de:

.. code::

    {
        "telefone" : {
            "ddd"    : 11,
            "numero" : 123456789
        }
    }

Em uma única propriedade ``phone`` do seu Domain, então o valor dessa propriedade após criada será ``11123456789``

Isso não se limita as propriedades dentro de uma mesma `key` você pode fazer algo como:

.. code-block:: php

    <?php
    class MeuDomain
    {
        /**
        * @var string $phone numero de telefone do cliente
        * @translate paises->codigos->brasil + cidades->saoPaulo->ddd + telefone->numero
        */
        public $phone;
    }

Se seu array/objeto for:

.. code::

    {
        "paises"   : {
            "codigos" : {
                "brasil" : "+55"
            }
        },
        "cidades"    : {
            "saoPaulo" : {
                "ddd" : 11
            }
        },
        "telefone" : {
            "numero" : 123456789
        }
    }


Então o valor de sua propriedade após criada será ``+5511123456789``
