.. _contributing:

Contribuindo
============
Se você está seguindo essa documentações, provavémente você pertence a uma das empresas do grupo Fintecc.
Sendo assim, qualuer contribuição para este projeto é bem vinda

Como contribuir
^^^^^^^^^^^^^^^
Você pode criar novas funcionálidades, novos componentes, novas classes, refatorar o código ou criar novas implementações,
tudo é bem vindo.

Caso você tenha acesso ao repositório diretamente, basta criar um novo branch e começar a escrever seus códigos,
quando tudo estiver pronto, basta fazer um pull-request para a master através do bitbucket.
Se você não tem acesso direto ao repositório, você criar um fork do projeto e então fazer o pull-request.

Escolha a forma que lhe for mais conveniente.

É importante que antes de começar a contribuir você leia :ref:`pratices` para ter certeza que sua contribuição será aceita.

Fazendo o pull-request
^^^^^^^^^^^^^^^^^^^^^^
Após efetuar sua alteração (seja ela por branch ou fork) basta ir até o site do bitbucket e criar um novo pull-request.

Caso não saiba como fazê-lo, ou deseja saber mais sobre pull-request veja a documentação do bitbucket para `criar um pull request`_


.. _`criar um pull request`: https://www.atlassian.com/git/tutorials/making-a-pull-request
