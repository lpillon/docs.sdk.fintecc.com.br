.. _services:

Serviços/conexões
=================
Um Service é uma módel de conexão com alguma API da Fintecc

Utilização
^^^^^^^^^^
Caso você esteja criando seu prórprio service você deve extender o service padrão da Fintecc,
e então implementar os métodos ``defaultApiName`` e ``defaultApiAuth``.
Esses métodos vão apenas dizer qual API e com qual autenticação sua conexão deve ser feita.

Esses métodos devem retornar uma string que faça referência as configurações que estão em ``Fintecc\config\fintecc.php``

Por exemplo, digamos que você tenha criado as URLs de sua nova conexão da seguinte forma:

.. code-block:: php

    <?php
    "api_url" => [
        // NEW COMPANTY
        "new_company" => [
            "local"      => "https://local.api.new_company.com.br/",
            "test"       => "https://tst.api.new_company.com.br/",
            "production" => "https://api.new_company.com.br/",
        ],

O método ``defaultApiName`` da sua classe deve retornar a ``string`` *new_company*.

Uma boa prática para os services (caso você esteja criando uma nova configuração no SDK)
é retornar uma constante definida em ``Fintecc\Interfaces\ServicesInterface``:

.. code-block:: php

    <?php
    namespace Fintecc\Interfaces;

    interface ServicesInterface
    {
        /** @var string API_NEW_COMPANY nome da api da new_company */
        const API_NEW_COMPANY = "new_company";

        //...
    }

Então sua classe ficaria assim:

.. code-block:: php

   <?php

    namespace NewCompany\Models;

    use Fintecc\Models\Service as FinteccService;

    class Service extends FinteccService
    {
        /** @inheritDoc */
        public function defaultApiName()
        {
            return static::API_NEW_COMPANY;
        }

        /** @inheritDoc */
        public function defaultApiAuth()
        {
            return static::AUTH_API;
        }
    }


Agora você pode utilizar todos os métodos HTTP como **GET** e **POST**, por exemplo,
que que a conexão será feita através dessas configurações

.. code-block:: php

    <?php
    $service = new NewCompany\Models\Service();
    $result = $service->get('/minha/url');

Alterando API e Autenticação
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Você pode alterar a API e a autenticação que seu service usa em tempo de execução com os métodos
``setApiName`` e ``setApiAuth`` que recebem como parametro uma ``string``
referência as configurações que estão em ``Fintecc\config\fintecc.php``

.. code-block:: php

    <?php
    $service = new NewCompany\Models\Service();
    $result = $service->get('/minha/url');

    $service->setApiAuth(static::AUTH_ADMIN);
    $result = $service->get('minha/url/de/admin');
