Fintecc SDK
===========

Essa é a documentação básica para começar a utilizar o Fintecc SDK.

.. note::
    Tenha em mente que é necessário ter acesso via ssh ao `Repositório`_ do SDK para utilizá-lo em seu projeto laravel.

.. toctree::
   :maxdepth: 2
   :caption: Geral:

   content/about

.. toctree::
   :maxdepth: 2
   :caption: Contribuindo:

   content/contributing
   content/pratices

.. toctree::
   :maxdepth: 2
   :caption: Utilização:

   content/instalation
   content/services
   content/authentication
   content/domain

.. _`Repositório`: https://bitbucket.org/ambientedevfintecc/sdk.fintecc.com.br
